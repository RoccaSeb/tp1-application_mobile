package com.example.app1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.app1.data.Country;

public class DetailFragment extends Fragment {

    public static final String TAG = "DetailFragment";
    TextView currency;
    TextView name;
    TextView capitale;
    ImageView imgView;
    TextView lang;
    TextView surface;
    TextView pop;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        capitale = view.findViewById(R.id.country_capital);
        name = view.findViewById(R.id.country_name);
        currency = view.findViewById(R.id.country_currency);
        lang = view.findViewById(R.id.country_lang);
        pop = view.findViewById(R.id.country_pop) ;
        surface = view.findViewById(R.id.country_surface ) ;
        imgView = view.findViewById(R.id.country_fl);


        //imgView = view.findViewById(R.id.country_flag);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());

        String uri = Country.countries[args.getCountryId()].getImgUri();
        Context c = imgView.getContext();
        imgView.setImageDrawable(c.getResources().getDrawable(c.getResources().getIdentifier(uri, null, c.getPackageName())));


        //textView.setText("Info in chapter "+(args.getCountryId()+1));
        name.setText(Country.countries[args.getCountryId()].getName());
        capitale.setText(Country.countries[args.getCountryId()].getCapital());
        currency.setText(Country.countries[args.getCountryId()].getCurrency());
        lang.setText(Country.countries[args.getCountryId()].getLanguage());
        pop.setText(Country.countries[args.getCountryId()].getPopulation()+ "");
        surface.setText(Country.countries[args.getCountryId()].getArea()+ " km2");

        //String uri = Country.countries[args.getCountryId()+1].getImgUri();
        //Context c = viewHolder.itemImage.getContext();
        //viewHolder.itemImage.setImageDrawable(c.getResources().getDrawable(c.getResources().getIdentifier(uri, null, c.getPackageName())));

        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }
}